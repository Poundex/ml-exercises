package com.example.ndarray;

import ai.djl.ndarray.NDArray;
import ai.djl.ndarray.NDManager;
import ai.djl.ndarray.types.DataType;
import ai.djl.ndarray.types.Shape;
import lombok.extern.slf4j.Slf4j;

import static com.example.ndarray.Util.get;
import static com.example.ndarray.Util.matrix;
import static com.example.ndarray.Util.vector;

@Slf4j
class Ex008TrafficLights3Deep {

	public static void main(String[] args) {
		NDManager manager = NDManager.newBaseManager();

		NDArray trafficLights = matrix(manager,
				vector(manager, 1, 0, 1),
				vector(manager, 0, 1, 1),
				vector(manager, 0, 0, 1),
				vector(manager, 1, 1, 1));

		NDArray truth = matrix(manager, vector(manager, 1, 1, 0, 0))
				.transpose();
		
		double alpha = 0.2;
		int hiddenSize = 4;

		NDArray weights_0_1 = manager.randomNormal(new Shape(3, hiddenSize), DataType.FLOAT64).mul(2).sub(1);
		NDArray weights_1_2 = manager.randomNormal(new Shape(hiddenSize, 1), DataType.FLOAT64).mul(2).sub(1);

		for (int iter = 0; iter < 60; iter++) {
			double layer2error = 0;
			for (int i = 0; i < trafficLights.size(0); i++) {
				NDArray layer0 = trafficLights.get(i).reshape(1, trafficLights.getShape().get(1));
				NDArray layer1 = relu(layer0.matMul(weights_0_1));
				NDArray layer2 = layer1.matMul(weights_1_2);

				layer2error += layer2.sub(truth.get(i).reshape(
						1, truth.getShape().get(1)))
						.pow(2)
						.sum()
						.getDouble();
						
				NDArray layer2delta = get(truth, i).sub(layer2).get(0);
				NDArray layer1delta = layer2delta
						.matMul(weights_1_2.transpose())
						.mul(reluToDerivative(layer1));

				weights_1_2 = weights_1_2
						.add(layer1.transpose()
								.matMul(layer2delta)
								.mul(alpha));

				weights_0_1 = weights_0_1
						.add(layer0.transpose()
								.matMul(layer1delta)
								.mul(alpha));

				if (iter % 10 == 9)
					log.atInfo().addKeyValue("error", layer2error).log();
			}
		}
	}

	public static double weightedSum(NDArray left, NDArray right) {
		return left.dot(right).getDouble();
	}
	
	public static NDArray relu(NDArray x) {
		return x.maximum(0);
	}
	
	public static NDArray reluToDerivative(NDArray x) {
		return x.gt(0);
	}
}
