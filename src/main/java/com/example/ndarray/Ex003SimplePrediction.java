package com.example.ndarray;

import ai.djl.ndarray.NDManager;
import lombok.extern.slf4j.Slf4j;

import java.util.stream.IntStream;

@Slf4j
class Ex003SimplePrediction {

	public static void main(String[] args) {
		NDManager manager = NDManager.newBaseManager();
		manager.getEngine().getDevices();

		double goal = 0.8;
		double initWeight = 0.5;
		double input = 0.5;
		double stepAmount = 0.001;

		IntStream.range(0, 1101)
				.mapToDouble(i -> i)
				.reduce(initWeight, (weight, ignored) -> {
					double prediction  = input * weight;
					double error = Math.pow(prediction - goal, 2);
					log.atInfo()
							.addKeyValue("error", error)
							.addKeyValue("prediction", prediction)
							.log();
					
					double upPrediction = input * (weight + stepAmount);
					double upError = Math.pow(goal - upPrediction, 2);
					
					double downPrediction = input * (weight - stepAmount);
					double downError = Math.pow(goal - downPrediction, 2);
					
					if(downError < upError)
						return weight - stepAmount;
					else if(downError > upError)
						return weight + stepAmount;
					else 
						return weight;
				});
	}
}
