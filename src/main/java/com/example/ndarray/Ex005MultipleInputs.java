package com.example.ndarray;

import ai.djl.ndarray.NDArray;
import ai.djl.ndarray.NDManager;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.stream.IntStream;

import static com.example.ndarray.Util.vector;

@Slf4j
class Ex005MultipleInputs {

	public static void main(String[] args) {
		NDManager manager = NDManager.newBaseManager();
		manager.getEngine().getDevices();

		NDArray numberOfToes = vector(manager, 8.5, 9.5, 10.0, 9.0);
		NDArray wlRec = vector(manager, 0.65, 0.8, 0.8, 0.9);
		NDArray numFans = vector(manager, 1.2, 1.3, 0.5, 1.0);
		NDArray winLoseGoal = vector(manager, 1, 1, 0, 1);
		NDArray initWeights = vector(manager, 0.1, 0.2, -0.1);
		
		double alpha = 0.01;
		double goal = winLoseGoal.getDouble(0);

		NDArray input = vector(manager, 
				numberOfToes.getDouble(0), 
				wlRec.getDouble(0), 
				numFans.getDouble(0));
		
		IntStream.range(0, 3)
				.boxed()
				.reduce(initWeights, (weights, iter) -> {
					double prediction  = neuralNetwork(input, weights);
					double delta = prediction - goal;
					double error = Math.pow(delta, 2);
					NDArray weightDeltas = input.mul(delta);
					
					log.atInfo()
							.addKeyValue("iteration", iter)
							.addKeyValue("prediction", prediction)
							.addKeyValue("error", error)
							.addKeyValue("delta", delta)
							.addKeyValue("weights", weights)
							.addKeyValue("weightDeltas", weightDeltas)
							.log();
					
					return vector(manager, Arrays.stream(weightDeltas.toDoubleArray())
							.map(weightDeltaAtIdx -> weightDeltaAtIdx * alpha)
							.toArray());
				},
						(l, r) -> r);
	}

	public static double neuralNetwork(NDArray input, NDArray weights) {
		return weightedSum(input, weights);
	}

	public static double weightedSum(NDArray left, NDArray right) {
		return left.dot(right).getDouble();
	}
}
