package com.example.ndarray;

import ai.djl.ndarray.NDArray;
import ai.djl.ndarray.NDManager;
import lombok.extern.slf4j.Slf4j;

import java.util.stream.IntStream;

import static com.example.ndarray.Util.matrix;
import static com.example.ndarray.Util.vector;

@Slf4j
class Ex006TrafficLights {

	public static void main(String[] args) {
		NDManager manager = NDManager.newBaseManager();
		manager.getEngine().getDevices();
		
		NDArray trafficLights = matrix(manager,
				vector(manager, 1, 0, 1),
				vector(manager, 1, 0, 1),
				vector(manager, 1, 0, 1),
				vector(manager, 1, 0, 1),
				vector(manager, 1, 0, 1),
				vector(manager, 1, 0, 1));
		
		NDArray truth = vector(manager, 0, 1, 0, 1, 1, 0);
				
		NDArray initWeights = vector(manager, 0.5, 0.48, -0.7);
		double alpha = 0.1;
		
		NDArray input = trafficLights.get(0);
		double goal = truth.getDouble(0);
		
		IntStream.range(0, 40)
				.boxed()
				.reduce(initWeights, (weights, iter) -> {
					double prediction = weightedSum(input, weights);
					double error = Math.pow(goal - prediction, 2);
					double delta = prediction - goal;
					
					log.atInfo()
							.addKeyValue("error", error)
							.addKeyValue("prediction", prediction)
							.log();
					
					return weights.sub(alpha * (input.mul(delta).getDouble(0)));
				}, (l, r) -> r);
	}

	public static double neuralNetwork(NDArray input, NDArray weights) {
		return weightedSum(input, weights);
	}

	public static double weightedSum(NDArray left, NDArray right) {
		return left.dot(right).getDouble();
	}
}
