package com.example.ndarray;

import ai.djl.ndarray.NDArray;
import ai.djl.ndarray.NDManager;

import java.util.Arrays;

class Util {
	public static NDArray vector(NDManager manager, double... doubles) {
		return manager.create(doubles);
	}
	
	public static NDArray vector(NDManager manager, double[]... doubles) {
		return manager.create(doubles);
	}
	
	public static NDArray matrix(NDManager manager, NDArray... vectors) {
		return manager.create(Arrays.stream(vectors)
				.map(NDArray::toDoubleArray)
				.toArray(double[][]::new));
	}
	
	public static NDArray get(NDArray x, int index) {
		NDArray ndArray = x.get(index);
		return ndArray.reshape(ndArray.getShape().get(0), x.getShape().get(1));
	}
}
