package com.example.ndarray;

import ai.djl.ndarray.NDManager;
import lombok.extern.slf4j.Slf4j;

import java.util.stream.IntStream;

@Slf4j
class Ex004SimplePredictionWithDirection {

	public static void main(String[] args) {
		NDManager manager = NDManager.newBaseManager();
		manager.getEngine().getDevices();

		double goal = 0.8;
		double initWeight = 0.5;
		double input = 2;
		double alpha = 0.1;

		IntStream.range(0, 20)
				.mapToDouble(i -> i)
				.reduce(initWeight, (weight, ignored) -> {
					double prediction  = input * weight;
					double error = Math.pow(prediction - goal, 2);
					double derivative = input * (prediction - goal);
					double newWeight = weight - (alpha * derivative);
					
					log.atInfo()
							.addKeyValue("error", error)
							.addKeyValue("prediction", prediction)
							.log();
					
					return newWeight;
				});
	}
}
