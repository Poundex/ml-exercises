package com.example.ndarray;

import ai.djl.ndarray.NDArray;
import ai.djl.ndarray.NDManager;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class Ex002FirstNeuralNetwork {

	public static void main(String[] args) {
		NDManager manager = NDManager.newBaseManager();
		NDArray weights = manager.create(new double[] { 0.1, 0.2, 0.0 });

		NDArray numberOfToes = manager.create(new double[] { 8.5, 9.5, 10.0, 9.0 });
		NDArray wlRec = manager.create(new double[] { 0.65, 0.8, 0.8, 0.9 });
		NDArray numFans = manager.create(new double[] { 1.2, 1.3, 0.5, 1.0});
		
		NDArray input = manager.create(new double[] {
				numberOfToes.getDouble(0),
				wlRec.getDouble(0),
				numFans.getDouble(0) });
		
		double prediction = neuralNetwork(input, weights);
		log.atInfo().addKeyValue("prediction", prediction).log();
	}
	
	public static double neuralNetwork(NDArray input, NDArray weights) {
		return weightedSum(input, weights);
	}
	
	public static double weightedSum(NDArray left, NDArray right) {
		return left.dot(right).getDouble();
	}
}
