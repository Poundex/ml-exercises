package com.example.ndarray;

import ai.djl.ndarray.NDArray;
import ai.djl.ndarray.NDManager;
import lombok.extern.slf4j.Slf4j;

import static com.example.ndarray.Util.matrix;
import static com.example.ndarray.Util.vector;

@Slf4j
class Ex007TrafficLights2 {

	public static void main(String[] args) {
		NDManager manager = NDManager.newBaseManager();
		manager.getEngine().getDevices();

		NDArray trafficLights = matrix(manager,
				vector(manager, 1, 0, 1),
				vector(manager, 0, 1, 1),
				vector(manager, 0, 0, 1),
				vector(manager, 1, 1, 1),
				vector(manager, 0, 1, 1),
				vector(manager, 1, 0, 1));

		NDArray truth = vector(manager, 0, 1, 0, 1, 1, 0);

		NDArray initWeights = vector(manager, 0.5, 0.48, -0.7);
		double alpha = 0.1;

		for (int i = 0; i < 40; i++) {
			double errorForAllLights = 0;
			for(int r = 0; r < truth.size(); r++) {
				NDArray input = trafficLights.get(r);
				double goal = truth.get(r).getDouble();
				double prediction = weightedSum(input, initWeights);
				double error = Math.pow(goal - prediction, 2);
				errorForAllLights += error;
				double delta = prediction - goal;
				initWeights = initWeights.sub(input.mul(delta).mul(alpha));
//				log.atInfo()
//						.addKeyValue("prediction", prediction)
//						.log();
			}
			log.atInfo()
					.addKeyValue("error", errorForAllLights)
					.log();
		}
	}

	public static double neuralNetwork(NDArray input, NDArray weights) {
		return weightedSum(input, weights);
	}

	public static double weightedSum(NDArray left, NDArray right) {
		return left.dot(right).getDouble();
	}
}
