package com.example.djl;

import ai.djl.Model;
import ai.djl.basicdataset.cv.classification.Mnist;
import ai.djl.engine.Engine;
import ai.djl.inference.Predictor;
import ai.djl.metric.Metrics;
import ai.djl.modality.Classifications;
import ai.djl.modality.cv.Image;
import ai.djl.modality.cv.ImageFactory;
import ai.djl.modality.cv.transform.ToTensor;
import ai.djl.modality.cv.translator.ImageClassificationTranslator;
import ai.djl.ndarray.NDList;
import ai.djl.ndarray.NDManager;
import ai.djl.ndarray.types.Shape;
import ai.djl.nn.Activation;
import ai.djl.nn.Blocks;
import ai.djl.nn.SequentialBlock;
import ai.djl.nn.core.Linear;
import ai.djl.training.DefaultTrainingConfig;
import ai.djl.training.EasyTrain;
import ai.djl.training.Trainer;
import ai.djl.training.TrainingResult;
import ai.djl.training.dataset.Dataset;
import ai.djl.training.evaluator.Accuracy;
import ai.djl.training.listener.SaveModelTrainingListener;
import ai.djl.training.listener.TrainingListener;
import ai.djl.training.loss.Loss;
import ai.djl.training.util.ProgressBar;
import ai.djl.translate.TranslateException;
import ai.djl.translate.Translator;
import lombok.Cleanup;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
class Ex009MnistRecognition {
	public static void main(String[] args) throws IOException, TranslateException {
		NDManager manager = NDManager.newBaseManager();

		Mnist train = Mnist.builder()
				.optUsage(Dataset.Usage.TRAIN)
				.setSampling(128, true)
				.build();
		Mnist test = Mnist.builder()
				.optUsage(Dataset.Usage.TEST)
				.setSampling(128, true)
				.build();
		train.prepare();
		train.prepare(new ProgressBar());
		test.prepare(new ProgressBar());

		SequentialBlock block = new SequentialBlock();
		block.add(Blocks.batchFlattenBlock(28 * 28));
		block.add(Linear.builder().setUnits(512).build());
		block.add(Activation.reluBlock());
		block.add(Linear.builder().setUnits(10).build());
		block.add(x -> new NDList(x.singletonOrThrow().logSoftmax(1)));

		Model model = Model.newInstance("modelName");
		model.setBlock(block);
		DefaultTrainingConfig config = setupTrainingConfig();

		@Cleanup Trainer trainer = model.newTrainer(config);
		trainer.setMetrics(new Metrics());

		Shape inputShape = new Shape(1, Mnist.IMAGE_HEIGHT * Mnist.IMAGE_WIDTH);
		trainer.initialize(inputShape);

		EasyTrain.fit(trainer, 5, train, null);
		log.atInfo()
				.addKeyValue("epoch", trainer.getTrainingResult().getEpoch())
				.addKeyValue("trainLoss", trainer.getTrainingResult().getTrainLoss())
				.log();

		///////////////////////

		Path imageFile = Paths.get("src/main/resources/new6.png");
		Image img = ImageFactory.getInstance().fromFile(imageFile);
		
		List<String> classes =
				IntStream.range(0, 10).mapToObj(String::valueOf).collect(Collectors.toList());
		Translator<Image, Classifications> translator =
				ImageClassificationTranslator.builder()
						.addTransform(new ToTensor())
						.optSynset(classes)
						.optApplySoftmax(true)
						.optTopK(1)
						.build();

		try (Predictor<Image, Classifications> predictor = model.newPredictor(translator)) {
			log.atInfo().addKeyValue("prediction", predictor.predict(img)).log();
		}
	}

	private static DefaultTrainingConfig setupTrainingConfig() throws IOException {
		String outputDir = Files.createTempDirectory("modelsandsuch").toAbsolutePath().toString();
		SaveModelTrainingListener listener = new SaveModelTrainingListener(outputDir);
		listener.setSaveModelCallback(
				trainer -> {
					TrainingResult result = trainer.getTrainingResult();
					Model model = trainer.getModel();
					Float accuracy = result.getValidateEvaluation("Accuracy");
					if(accuracy == null) return;
					model.setProperty("Accuracy", String.format("%.5f", accuracy));
					model.setProperty("Loss", String.format("%.5f", result.getValidateLoss()));
				});
		
		return new DefaultTrainingConfig(Loss.softmaxCrossEntropyLoss())
				.addEvaluator(new Accuracy())
				.optDevices(Engine.getInstance().getDevices(1))
				.addTrainingListeners(TrainingListener.Defaults.logging(outputDir))
				.addTrainingListeners(listener);
	}
}
