package com.example.djl;

import ai.djl.Model;
import ai.djl.engine.Engine;
import ai.djl.metric.Metrics;
import ai.djl.ndarray.NDManager;
import ai.djl.nn.Activation;
import ai.djl.nn.Blocks;
import ai.djl.nn.SequentialBlock;
import ai.djl.nn.core.Linear;
import ai.djl.training.DefaultTrainingConfig;
import ai.djl.training.EasyTrain;
import ai.djl.training.Trainer;
import ai.djl.training.TrainingResult;
import ai.djl.training.dataset.Dataset;
import ai.djl.training.evaluator.Accuracy;
import ai.djl.training.listener.SaveModelTrainingListener;
import ai.djl.training.listener.TrainingListener;
import ai.djl.training.loss.Loss;
import ai.djl.training.optimizer.Optimizer;
import ai.djl.translate.TranslateException;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;

@Slf4j
class Ex010ImdbClassification {
	public static void main(String[] args) throws IOException, TranslateException {
		NDManager manager = NDManager.newBaseManager();

//		StanfordMovieReview train = StanfordMovieReview.builder()
//				.optUsage(Dataset.Usage.TRAIN)
//				.build();
//		StanfordMovieReview test = StanfordMovieReview.builder()
//				.optUsage(Dataset.Usage.TEST)
//				.build();
//
//		train.prepare(new ProgressBar());
//		test.prepare(new ProgressBar());


		ImdbDataset imdbDataset = new ImdbDataset(10_000, manager);
		Dataset trainDataset = imdbDataset.getTrainDataset();
		Dataset validationDataset = imdbDataset.getValidationDataset();
		Dataset testDataset = imdbDataset.getTestDataset();

		SequentialBlock block = new SequentialBlock();
		block.add(Blocks.batchFlattenBlock(512));
		
		block.add(Linear.builder().setUnits(16).build());
		block.add(Activation.reluBlock());
		
		block.add(Linear.builder().setUnits(16).build());
		block.add(Activation.reluBlock());
		
		block.add(Activation.sigmoidBlock());

		Model model = Model.newInstance("modelName");
		model.setBlock(block);
		DefaultTrainingConfig config = setupTrainingConfig();

		Trainer trainer = model.newTrainer(config);
		trainer.setMetrics(new Metrics());

		EasyTrain.fit(trainer, 20, trainDataset, validationDataset);
		log.atInfo()
				.addKeyValue("result", trainer.getTrainingResult())
				.log();
		
		trainer.close();
	}

	private static DefaultTrainingConfig setupTrainingConfig() throws IOException {
		String outputDir = Files.createTempDirectory("modelsandsuch").toAbsolutePath().toString();
		
		SaveModelTrainingListener listener = new SaveModelTrainingListener(outputDir);
		listener.setSaveModelCallback(
				trainer -> {
					TrainingResult result = trainer.getTrainingResult();
					Model model = trainer.getModel();
					Float accuracy = result.getValidateEvaluation("Accuracy");
					if(accuracy == null) return;
					model.setProperty("Accuracy", String.format("%.5f", accuracy));
					model.setProperty("Loss", String.format("%.5f", result.getValidateLoss()));
				});
		
		return new DefaultTrainingConfig(Loss.sigmoidBinaryCrossEntropyLoss())
				.addEvaluator(new Accuracy())
				.optOptimizer(Optimizer.rmsprop().build())
				.optDevices(Engine.getInstance().getDevices(1))
				.addTrainingListeners(TrainingListener.Defaults.logging(outputDir))
				.addTrainingListeners(listener);
	}
}
