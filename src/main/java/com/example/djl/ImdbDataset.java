package com.example.djl;

import ai.djl.ndarray.NDArray;
import ai.djl.ndarray.NDList;
import ai.djl.ndarray.NDManager;
import ai.djl.ndarray.index.NDIndex;
import ai.djl.ndarray.types.Shape;
import ai.djl.training.dataset.Batch;
import ai.djl.training.dataset.Dataset;
import ai.djl.util.Progress;
import lombok.Getter;
import net.poundex.xmachinery.fio.io.DefaultIo;
import net.poundex.xmachinery.fio.io.IO;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
class ImdbDataset {

	private static final IO fio = new DefaultIo();

	private final int numWords;
	private final NDManager manager;
	
	private final BidiMap<String, Integer> wordsToIndex;
	private final Map<Integer, String> indexToWords;
	
	private final List<Review> trainData;
	private final List<Review> testData;
	
	record Review(String normalisedText, int[] indexised, boolean positive, int stars) {
		public int positiveInt() {
			return positive ? 1 : 0;
		}
	};

	public ImdbDataset(int numWords, NDManager manager) {
		this.numWords = numWords;
		this.manager = manager;

		Path path = Paths.get("/home/pounder/Downloads/imdb-dataset/aclImdb/");
		Path vocab = path.resolve("imdb.vocab");
		AtomicInteger idx = new AtomicInteger();

		wordsToIndex = Stream.concat(
						Stream.of("<P>", "<S>", "<unk>"),
						fio.lines(vocab).limit(numWords))
				.collect(Collectors.toMap(
						kv -> kv,
						ignored -> idx.getAndIncrement(),
						(l, r) -> { throw new AssertionError("Can't happen: duplicate word index"); },
						DualHashBidiMap::new));
		indexToWords = wordsToIndex.inverseBidiMap();

		trainData = Stream.concat(
						readReviews(path.resolve("train/pos/"), true),
						readReviews(path.resolve("train/pos/"), false))
				.toList();

		testData = Stream.concat(
						readReviews(path.resolve("test/pos/"), true),
						readReviews(path.resolve("test/pos/"), false))
				.toList();
	}

	private Stream<Review> readReviews(Path resolve, boolean positive) {
		return fio.list(resolve)
				.map(p -> {
					String filename = p.getFileName().toString();
					String stars = filename.substring(filename.lastIndexOf("_") + 1, filename.lastIndexOf("."));
					String review = fio.readString(p);
					String normalisedReview = normalise(review);
					int[] indexised = Stream.concat(
									Stream.of(1),
									Arrays.stream(normalisedReview.split(" "))
											.map(word -> wordsToIndex.getOrDefault(word, 2)))
							.mapToInt(i -> i)
							.toArray();
					
					return new Review(normalisedReview, indexised, positive, Integer.parseInt(stars));
				});
	}

	private String normalise(String review) {
		return review
				.replaceAll("<br />", "")
				.replaceAll("[^a-zA-Z ]", "")
				.toLowerCase();
	}
	
	public NDArray vectorise(int[] array) {
		NDArray result = manager.zeros(new Shape(numWords + 3));
		Arrays.stream(array).forEach(wordIdx -> result.set(new NDIndex(wordIdx), 1));
		return result;
	}
	
	public Dataset getTrainDataset() {
		return toDataset(io.vavr.collection.Stream.ofAll(trainData).take(trainData.size() - 10_000));
	}

	public Dataset getValidationDataset() {
		return toDataset(io.vavr.collection.Stream.ofAll(trainData).drop(trainData.size() - 10_000));
	}

	public Dataset getTestDataset() {
		return toDataset(io.vavr.collection.Stream.ofAll(testData));
	}

	private Dataset toDataset(io.vavr.collection.Stream<Review> stream) {
		return new Dataset() {
			@Override
			public Iterable<Batch> getData(NDManager manager) {
				return () -> stream
						.grouped(512)
						.map(x -> {
							NDList data = new NDList(512);
							NDList labels = new NDList(512);
							x.forEach(review -> {
								data.add(vectorise(review.indexised()));
								labels.add(manager.create(review.positiveInt()));
							});
							
							return new Batch(
									manager,
									data,
									labels,
									512,
									null,
									null,
									0,
									0);
						});
			}

			@Override
			public void prepare(Progress progress) { }
		};
	}
}
