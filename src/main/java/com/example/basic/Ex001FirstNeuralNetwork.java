package com.example.basic;

import io.vavr.collection.Stream;
import lombok.extern.slf4j.Slf4j;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
class Ex001FirstNeuralNetwork {

	public static void main(String[] args) {
		double[] weights = new double[] { 0.1, 0.2, 0.0 };
		
		double[] numberOfToes = new double[] { 8.5, 9.5, 10.0, 9.0 };
		double[] wlRec = new double[] { 0.65, 0.8, 0.8, 0.9 };
		double[] numFans = new double[] { 1.2, 1.3, 0.5, 1.0 };
		
		double[] input = new double[] { numberOfToes[0], wlRec[0], numFans[0] };
		double prediction = neuralNetwork(input, weights);
		
		log.atInfo().addKeyValue("prediction", prediction).log();
	}
	
	public static double neuralNetwork(double[] input, double[] weights) {
		return weightedSum(input, weights);
	}
	
	public static double weightedSum(double[] a, double[] b) {
		assertThat(a).hasSameSizeAs(b);

		return Stream.ofAll(a).zip(Stream.ofAll(b))
				.toJavaStream()
				.mapToDouble(t -> t._1() * t._2())
				.reduce(0, Double::sum);
	}
}
